# demo

stockengine public demo stuff & etc

to use:
 1. download compose and secrets
    1. ```wget -nc https://gitlab.com/stockengine-public/demo/-/archive/master/demo-master.zip```
    1. ```unzip demo-master.zip```
 1. change working directory
    ```
    cd demo-master
    ```
 1. startup cluster:
    1. ```docker-compose pull && docker-compose up```
    1. in case of errors (services starting in parallel, so in first time may be race - services starts prior first time init will be done) stop (ctrl+C) and start in again  
 1. connect to internal cli and create user:
    1. ```docker exec -it demo-master_se.cli_1 /opt/stockengine/se.cli```
    1. ```Auth Connect -Addr=se.auth:7111```
    1. ```Auth CreateUpdatePassword -Login=test123 -Pass=123```
    1. ```exit```
 1. now you may create auth token via external http service
    1. ```curl -v -d'{"Login":"test123","Password":"123"}' http://localhost:8080/auth/login```
    1. token will be in header Authorization and in json response body field "Auth" 
        ```
        {"Auth":"v2.public.Cgd0ZXN0MTIzEPqZ2e8FGPqktO8FINOlm-G-qpnzBygBMADWJxnym1_n_3DGTe6TPQenkyNcvBZ7bEeGrpmF7ORfMZatwPUslExZ2_saBW0ta_K1-JeRepOXEHglJuGeg-cF.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2NDIyNjUwLCJJc3N1ZWRBdCI6MTU3NTgxNzg1MCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0"}
        ```
    1. last part (after last dot) contain base64 encoded auth data
        ```
        echo 'eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2NDIyNjUwLCJJc3N1ZWRBdCI6MTU3NTgxNzg1MCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0' | base64 -d
        {"Login":"test123","Expiration":1576422650,"IssuedAt":1575817850,"Owner":569253814027604691,"Role":1,"Generation":0}
        ```  
 1. get access token (and new auth token - it is one-time and must be renew on access token expiration)
    1. ```curl -XPOST -v -H"Authorization: Bearer v2.public.Cgd0ZXN0MTIzEPqZ2e8FGPqktO8FINOlm-G-qpnzBygBMADWJxnym1_n_3DGTe6TPQenkyNcvBZ7bEeGrpmF7ORfMZatwPUslExZ2_saBW0ta_K1-JeRepOXEHglJuGeg-cF.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2NDIyNjUwLCJJc3N1ZWRBdCI6MTU3NTgxNzg1MCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0" http://localhost:8080/auth/refresh```
    1. new auth (refresh) token will be in Authorization header and Auth field of body, access token will be in X-Access-Token and Access field
        ```
        < Authorization: Bearer v2.public.Cgd0ZXN0MTIzEPqZ2e8FGKimtO8FINOlm-G-qpnzBygBMAExEIbxMHZcGTupcc0RNquAzEqIyfB_v3cTGXQS6k5GxGUQPrGyUdBi5HcpBva_EhnOj2r0tysOulj8aF7G9BcB.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2NDIyNjUwLCJJc3N1ZWRBdCI6MTU3NTgxODAyNCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MX0
        < X-Access-Token: v2.public.CNOlm-G-qpnzBxDUqLTvBRioprTvBSABtWtb99Dvz9W_JkwZj3LjpfPKtVWjvhrl6I-REIjzFMjDnryv_g1FfWIe-rFftkckHDTZ39TC2tF9fjgGrDieBw
        {"Access":"v2.public.CNOlm-G-qpnzBxDUqLTvBRioprTvBSABtWtb99Dvz9W_JkwZj3LjpfPKtVWjvhrl6I-REIjzFMjDnryv_g1FfWIe-rFftkckHDTZ39TC2tF9fjgGrDieBw","Auth":"v2.public.Cgd0ZXN0MTIzEPqZ2e8FGKimtO8FINOlm-G-qpnzBygBMAExEIbxMHZcGTupcc0RNquAzEqIyfB_v3cTGXQS6k5GxGUQPrGyUdBi5HcpBva_EhnOj2r0tysOulj8aF7G9BcB.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2NDIyNjUwLCJJc3N1ZWRBdCI6MTU3NTgxODAyNCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MX0"}
        ```
1.  and lets check access token (it will be expired on 5 minute)
    1. ```curl -v -H'X-Access-Token: v2.public.CNOlm-G-qpnzBxDUqLTvBRioprTvBSABtWtb99Dvz9W_JkwZj3LjpfPKtVWjvhrl6I-REIjzFMjDnryv_g1FfWIe-rFftkckHDTZ39TC2tF9fjgGrDieBw' http://localhost:8080/auth/show```
    1. response will contain token info
        ```
        {"Owner":569253814027604691,"Expiration":1575818324,"IssuedAt":1575818024,"Role":1}
        ```
1. ```POST /rest/order```
   1. ```{"New":{"Type":"Limit","Operation":"Buy","Amount":9876543210.01234567,"Price":1234567890.0987654321,"ExtID":3456789}}```
   1. ```{"Edit":{"ExtID":456789,"NewAmount":9876543210.01234567,"NewPrice":1234567890.0987654321}}```
   1. ```{"Delete":{"ExtID":456789}}```
1. ```GET /rest/dom/top100```
   ```
   {"CommitID":2,"Bid":null,"Ask":[{"Count":2,"Price":1234567890.0987654321,"Amount":19753086420.02469134}]}
   ```

